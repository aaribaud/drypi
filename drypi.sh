#!/bin/bash
#
# drypi - a script to reduce the size of a Raspberry Pi image file
# without requiring root privileges or sudo.
#
# It does require some tools though:
# - guestfish
# - parted
# - grep
# - awk
# - ... and, of course, bash.

usage() {
    echo -e "\nUsage:"
    echo -e "    $0 [option [option[...]]] image-file new-image-file\n"
    echo -e "Where:"
    echo -e "    image-file is an existing Raspberry Pi image file"
    echo -e "    and new-image-file is the name for the new, dried image file"
    echo -e "    Options"
    echo -e "        -m:               minimize by runnning resize2fs -M several times"
    echo -e "        -r:               (Raspberry Pi OS) expand root partition on first boot"
    echo -e "        -R:               (Debian) expand root partition on first boot"
    echo -e "        -q once:          be quiet about multiple resize2fs runs"
    echo -e "        -q twice or more: be quiet about everything except errors"
    echo ""
}

# Check dependencies. This is always silent except for errors.

if [ "$(which guestfish)" == "" ]; then
    echo "*** ERROR: drypi.sh needs guestfish. Aborting."
    exit 1
fi

if [ "$(which virt-edit)" == "" ]; then
    echo "*** ERROR: drypi.sh needs virt-edit. Aborting."
    exit 1
fi

PARTED=$(which parted)
if [ "$PARTED" == "" ]; then
    PARTED=$(which /usr/sbin/parted)
fi
if [ "$PARTED" == "" ]; then
    PARTED=$(which /sbin/parted)
fi
if [ "$PARTED" == "" ]; then
    echo "*** ERROR: drypi.sh needs parted. Aborting."
    exit 1
fi

if [ "$(which grep)" == "" ]; then
    echo "*** ERROR: drypi.sh needs grep. Aborting."
    exit 1
fi

if [ "$(which awk)" == "" ]; then
    echo "*** ERROR: drypi.sh needs awk. Aborting."
    exit 1
fi

# Process options.

QUIET=0
while getopts ":mrRq" option; do
    case "${option}" in
        m)
            MINIMIZE="y"
            ;;
        r)
	    if [ ! -z "$FORCE_RESIZE" ]; then
		usage
		exit 1
	    else
		FORCE_RESIZE="rpios"
	    fi
            ;;
        R)
	    if [ ! -z "$FORCE_RESIZE" ]; then
		usage
		exit 1
	    else
		FORCE_RESIZE="debian"
	    fi
            ;;
        q)
            QUIET=$((QUIET+1))
            ;;
        *)
            usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

if [[ $# != 2 ]]; then
    usage
    exit 1
fi

if [[ -e $2 ]]; then
    echo "*** ERROR - File '$2' should not exist! Aborting."
    exit 1
fi

if [[ ! -f $1 ]]; then
    echo "*** ERROR - '$1' should be an existing and readable file! Aborting."
    exit 1
fi

# Let's get to work on our (existing) source and (future) target.

SOURCE="$1"
TARGET="$2"

# Check source partitions. Operate read-only.

if [[ $QUIET < 2 ]]; then
    echo "Checking $SOURCE partitions..."
fi

if [[ "$(guestfish --ro -a $SOURCE run : list-filesystems | head -1)" != "/dev/sda1: vfat" ]]; then
    echo "First partition of '$SOURCE' is not VFAT! Aborting."
    exit 1
fi

if [[ ! "$(guestfish --ro -a $SOURCE run : list-filesystems | tail +2 | head -1)" =~ "/dev/sda2: ext" ]]; then
    echo "Second partition of '$SOURCE' is not ext2/3/4! Aborting."
    exit 1
fi

if [[ ! "$(guestfish --ro -a $SOURCE run : list-filesystems | tail +3)" =~ "" ]]; then
    echo "There are more than 2 partitions in '$SOURCE'! Aborting."
    exit 1
fi

# Copy source to target.

if [[ $QUIET < 2 ]]; then
    echo "Copying $SOURCE to $TARGET..."
fi

if ! cp "$SOURCE" "$TARGET"; then
    echo "Could not copy image '$SOURCE' into '$TARGET' - Aborting."
    exit 1
fi

# Check and auto-repair rootfs partition, otherwise resize would fail.

if [[ $QUIET < 2 ]]; then
    echo "Checking root filesystem in $TARGET..."
fi

if ! guestfish -a $TARGET run : e2fsck /dev/sda2 forceall:yes; then
    echo "Could not check root partition in '$TARGET' - Aborting."
    exit 1
fi

# If requested, enforce resize on next boot.

if [ ! -z "$FORCE_RESIZE" ]; then

    case $FORCE_RESIZE in
        rpios)
	    if [[ $QUIET < 2 ]]; then
		echo "Enforcing rootfs resize on next Raspberry Pi OS boot in $TARGET..."
	    fi
            # Remove any old init= option and append new one.
            # The two intermediate "s/.../" are to remove double whitespaces
            # and start and end whitespaces which the removal may have created.
            if ! virt-edit -a $TARGET -m /dev/sda1:/ /cmdline.txt -e \
                    's/init=[^\s]+//g; s/\s+/ /g; s/^\s|\s$//g; $_ .= " init=/usr/lib/raspi-config/init_resize.sh"'; then
                echo "Could not append init= option into /boot/cmdline.txt in '$TARGET' - Aborting."
                exit 1
            fi
            ;;
        debian)
	    if [[ $QUIET < 2 ]]; then
		echo "Enforcing rootfs resize on next Debian boot in $TARGET..."
	    fi
            # Debian does the initial resize from within its initrd, but we won't rebuild a whole
            # initrd just for that, and expanding a mounted rootfs works just fine, so we simply
	    # create a script under /etc/boot.d that does the job then removes itself (but leaves
	    # /etc/boot.d/, sorry).
            guestfish -a $TARGET -m /dev/sda2:/ mkdir-p /etc/boot.d
            guestfish -a $TARGET -m /dev/sda2:/ upload - /etc/boot.d/undrypi <<-EOU
		#! /bin/sh
		# Find the root partition device path, device name, partition, and parent device
		ROOTFSPARTPATH=\$(findmnt -no SOURCE /)
		ROOTFSPARTDEV=\$(basename \$ROOTFSPARTPATH)
		ROOTFSPARTNO=\$(cat /sys/class/block/\$ROOTFSPARTDEV/partition)
		ROOTFSPARENTDEV=/dev/\$(lsblk -no pkname \$ROOTFSPARTPATH)
		# Extend the rootfs partition on the root device, don't complain too much
		sfdisk -f --no-reread --no-tell-kernel \$ROOTFSPARENTDEV -N \$ROOTFSPARTNO <<EOF
		,+
		EOF
		# Update kernel partition tables
		partprobe
		# Resize the rootfs inside the root partition
		resize2fs \$ROOTFSPARTPATH
		rm -f /etc/boot.d/undrypi
		EOU
            guestfish -a $TARGET -m /dev/sda2:/ chmod 0755 /etc/boot.d/undrypi
            ;;
    esac
fi

# Resize rootfs partition.

if [[ $QUIET < 2 ]]; then
    echo "Shrinking root filesystem inside $TARGET..."
fi

# Get rootfs partition size in blocks.
ROOTBLOCKCOUNT=$(guestfish -a $TARGET run : tune2fs-l /dev/sda2 | grep '^Block count:' | awk '{ print $3 }')

# Actually resize rootfs partition once, or if -m, until minimized.
PREVROOTBLOCKCOUNT=0
while [ $ROOTBLOCKCOUNT != $PREVROOTBLOCKCOUNT ]; do

    if [[ $QUIET < 3 ]]; then
        echo "Current $TARGET root filesystem size: $ROOTBLOCKCOUNT blocks"
    fi

    guestfish -a $TARGET run : resize2fs-M /dev/sda2

    PREVROOTBLOCKCOUNT=$ROOTBLOCKCOUNT
    # Recompute rootfs partition in blocks.
    ROOTBLOCKCOUNT=$(guestfish -a $TARGET run : tune2fs-l /dev/sda2 | grep '^Block count:' | awk '{ print $3 }')

    # If we were not asked to minimize, exit after first iteration.
    if [[ "$MINIMIZE" != "y" ]]; then
        break
    fi
done

if [[ $QUIET < 2 ]]; then
    echo "Final $TARGET root filesystem size: $ROOTBLOCKCOUNT blocks"
fi

# Check and auto-repair shrunk rootfs partition.

if [[ $QUIET < 2 ]]; then
    echo "Checking shrunk root filesystem inside rootfs of $TARGET..."
fi

guestfish -a $TARGET run : e2fsck /dev/sda2 forceall:yes

# Recompute and set partition 2 end (last byte used).

if [[ $QUIET < 2 ]]; then
    echo "Shrinking rootfs partition inside $TARGET ..."
fi

# Get block size of root filesystem on rootfs partition.
ROOTBLOCKSIZE=$(guestfish -a $TARGET run : tune2fs-l /dev/sda2 | grep '^Block size:' | awk '{ print $3 }')

# Get rootfs partition start in bytes.
SDA2START=$($PARTED -s $TARGET unit b print | grep '^\s\+2\b' | awk '{ print $2 }' | tr -d 'B')

# Compute new rootfs partition end in bytes.
NEWSDA2END=$((SDA2START + ROOTBLOCKCOUNT * ROOTBLOCKSIZE - 1))

$PARTED -s $TARGET unit b rm 2 mkpart primary ext4 $SDA2START $NEWSDA2END

# Truncate target file.

if [[ $QUIET < 2 ]]; then
    echo "Truncating $TARGET..."
fi

# Compute new file size in bytes.
NEWIMGSIZE=$((SDA2START + ROOTBLOCKCOUNT * ROOTBLOCKSIZE))

truncate $TARGET -s $NEWIMGSIZE

# Done.

if [[ $QUIET < 2 ]]; then
    echo "All done."
fi
