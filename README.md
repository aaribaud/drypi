# DryPi

**IMPORTANT NOTICE:**

> **This project has moved to [Codeberg](https://codeberg.org/aaribaud/drypi).**
> **The present repository will NOT receive further updates.**

This Bash script will shrink a Raspberry Pi image as much as possible
in order to minimize storage and transfer time.

It differs from other similar scripts in that you don't need `root` or
`sudo` to run it; instead you will need
[libguestfs](https://libguestfs.org)., which will provide the
`guestfish` and `virt-edit` tools needed by `drypi.sh`.

If you don't have `libguestfs` installed, then maybe you should consider
*not* using `drypi.sh`, as installing `libguests` will pull quite a few
dependencies which apparently you haven't needed so far, and it might be
simpler for you to just live with having to enter the `root` or `sudo`
password from time to time.

But if you *do* envision working on tweaking Raspberry Pi images (or
virtual machine disk images really), then libguest is a must-have anyway
as it provides a lot more than just tweaking image size; it lets you do
just about anything on Raspberry Pi images (or, again, virtual machine
disk images), such as adding modifying or removing files, all without
having to go `sudo` or `root`.

This README will *not* explain how to install `libguestfs`.
You'll have to refer to your OS of choice for that.

Note that `drypi.sh` also needs `parted`, `grep`, `awk`, and
obviously `bash`, and is assumed to be run in a terminal,
although as far as I can tell it should run fine from a script;
notably, it never prompts interactively for user answers.

Anyway, if any of its dependencies is not accessible, `drypi.sh` will
complain about it.

Also, *please* go read the FAQ section at the end of this document.

## Installation

Place `drypi.sh` wherever you prefer on your `$PATH`.
You can place it in `/usr/local/bin` if several users on the machine
will use it, or in a per-user location such as `~/.local/bin` if only
you will use it.

## Usage

`drypi.sh [options] source.img target.img`

options can be displayed by running `drypi.sh` without arguments,
and are (hopefully) pretty self-explanatory.

`source.img` is an existing Raspberry Pi image file, usually copied from
an SD card (you could copy it from a full hard drive, but... you're
gonna need a bigger drive).

`target.img` is the name of a *nonexistent* file in which `drypi.sh`
will put the shrunk copy of `source.img`.

If a file, directory, socket, or other... thing already exists under the
`target.img` name, `drypi.sh` will abort (see FAQ below).

`drypi.sh` makes *some* effort to check that the image you're working on
is indeed a Raspberry Pi image:

- there must be exactly two partitions
- first partition must be VFAT
- second partition must be ext2/3/4

But that's about all!

## Frequently Asked Questions

### Alright. Why the name?

Well, if you *dry* a *pie*, it will shrink somewhat.

(the *.sh* part is because it's a shell script, if anyone wonders.)

### Why does libguestfs complain that /usr/bin/supermin exited with error 1?

Because you're running Ubuntu, or some other distro which prevents
non-root users from even *reading* some files in /boot/, among which
all vmlinuz-* files, including the one matching the currently running
kernel... which libguestfs needs to be able to read.

Such a restrinction is:

1. Useless: if a local user is eager to read the content of /boot/vmlinuz,
   they just need to manually fetch the *publically available*
   corresponding package and just extract the file without even needing
   to become `root`.

2. Easily fixable: by opening a shell (who am I kidding, you *already*
   have a shell open since you're trying to run `drypi.sh`) and running
   the following command:

   `sudo chmod a+r /root/*` (or `su -`, `chmod a+r /root/*`, and `exit`)

   Granted, it'll ask for your own or admin password, but only once...
   until the next time you upgrade your system's kernel.

### What is the option to overwrite the target file if it already exists?

There is *no* such option, and there *will not be*; this is so that you
cannot accidentally damage an existing file with `drypi.sh`.

If you need to overwrite the target file, you can do that with an
`rm` command (with option `-f` if you're daring) before invoking `drypi.sh`.

### How do I make `drypi.sh` *enlarge*, rather than *shrink*, the rootfs?

You don't.

That's because there's already a tool in `libguestfs` which makes it
possible to enlarge an image with just two commands: `virt-resize`.

The following:

`truncate -s 8G target.img`
`virt-resize source.img target.img`

Will create a target file with a overall size of 8 GB and a rootfs
enlarged to occupy as much of it as possible.

### So `drypi.sh` just wraps the `virt-resize` `--shrink` option?

Nope.

This option will shrink the *partition*, but you'll have to shrink the
*filesystem* in the partition (using `resize2fs`) before running
`virt-resize`, and *after running it* you'll have to truncate the
*image file* (using, well, `truncate`).

That's the added value of `drypi.sh`: it does all that for you, plus
it can optionally repeat the shrink (`resize2fs`) phase until there
really is no space left to remove.

### What else does `drypi.sh` do beside resizing the image?

If your image is Raspberry Pi OS one, then `drypi.sh` can optionally
set things up so that the rootfs will be automatically re-enlargeed to
fit the actual SD, HDD or eMMC it is run on (that is the blue dialog
you see for five seconds when booting the Raspberry Pi OS for the very
first time).

If your image is a Debian one, then `drypi.sh` can *also* optionally
do this (without the blue screen and additional reboot).

(If you must know, this is done using `virt-edit` from... `libguestfs`.)

### I've got a question not covered in this section.

Feel free to head over to the project's
[`issues`](https://gitlab.com/aaribaud/drypi/-/issues)
and ask!
